//  Vuetify colors
import colors from 'vuetify/es5/util/colors'

export default {
    primary: colors.blue.lighten1,
    secondary: colors.grey.darken2,
    accent: colors.blue.accent1,
    error: colors.red,
    info: colors.blue,
    success: colors.green,
    warning: colors.orange
}