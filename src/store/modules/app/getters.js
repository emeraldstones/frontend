import { get } from '@/utils/vuex'

export default {
    getDrawer: get('drawer'),
    getDrawerBackground: get('drawerBackground'),
    getSelectedDataTableCategory: get('selectedDataTableCategory'),
    getAlertVisibility: get('alertVisible'),
    getAlertType: get('alertType'),
    getAlertMessage: get('alertMessage'),
    getProgress: get('progress'),
    getRouteChanging: get('routeChanging'),
    getAppointmentDialog: get('appointmentDialog')
}
