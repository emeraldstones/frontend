export default {
    drawer: null,
    drawerBackground: '',
    severity: 4,
    selectedDataTableCategory: null,
    alertVisible: false,
    alertType: 'success',
    alertMessage: '',
    progress: false,
    appointmentDialog: false,
    routeChanging: false,
}
