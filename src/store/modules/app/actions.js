export default {
    toggleDrawer({ commit }) {
        commit('TOOGLE_DRAWER')
    },

    setDrawer({ commit }, value) {
        commit('SET_DRAWER', value)
    },

    setRouteChanging({ commit }, value) {
        commit('SET_ROUTE_CHANGING', value)
    },

    setSelectedDataTableCategory({ commit }, value) {
        commit('SET_SELECTED_DATA_TABLE_CATEGORY', value)
    },

    setAlert({ commit }, alert) {
        commit('SET_ALERT', alert)

        setTimeout(function() {
            commit('SET_ALERT', {
                visible: false,
                type: 'success',
                message: '',
            })
        }, 6000)
    },

    setProgress({ commit }, value) {
        commit('SET_PROGRESS', value)
    },

    setAppointmentDialog({ commit }, value) {
        commit('SET_APPOINTMENT_DIALOG', value)
    },

    setSeverity({ commit }, value) {
        commit('SET_SEVERITY', value)
    },

    updateSeverity({ commit, dispatch, rootGetters }, value) {
        let severity = 4;
        rootGetters['dashboard/getMonitoringPatients'].forEach(patient => {
            //  SEVERITY: III
            if (
                patient.temperature >= 38 && patient.temperature <= 39.5 ||
                patient.temperature > 35.5 && patient.temperature < 36 &&
                severity > 3
            ) {
                severity = 3
            }
            //  SEVERITY: II
            if (
                patient.temperature > 39.5 && patient.temperature <= 41 ||
                patient.temperature >= 34 && patient.temperature <= 35.5 &&
                severity > 2
            ) {
                severity = 2
            }
            //  SEVERITY: I
            if (
                patient.temperature > 41 ||
                patient.temperature < 34 &&
                severity > 1
            ) {
                severity = 1
            }
        })
        dispatch('setSeverity', severity)
    }
}
