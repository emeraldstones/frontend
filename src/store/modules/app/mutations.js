import { set, toggle } from '@/utils/vuex'

export default {
    SET_SEVERITY: set('severity'),
    SET_DRAWER: set('drawer'),
    SET_SELECTED_DATA_TABLE_CATEGORY: set('selectedDataTableCategory'),
    SET_PROGRESS: set('progress'),
    SET_ROUTE_CHANGING: set('routeChanging'),
    SET_APPOINTMENT_DIALOG: set('appointmentDialog'),
    SET_ALERT(state, alert) {
        state.alertVisible = alert.visible
        state.alertType = alert.type
        state.alertMessage = alert.message
    },
    TOOGLE_DRAWER: toggle('drawer')
}
