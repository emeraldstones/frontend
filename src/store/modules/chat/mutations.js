import { set } from '@/utils/vuex'

export default {
    SET_CONTACTS: set('contacts'),
}