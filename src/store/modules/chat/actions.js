import axios from 'axios'
import { refresh, expired } from '@/utils/api'

export default {
    fetchContacts({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('chat/contacts')
            .then(response => {
                commit('SET_CONTACTS', response.data.contacts)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },
}