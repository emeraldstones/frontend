import { get } from '@/utils/vuex'

export default {
    getContacts: get('contacts')
}