import { get } from '@/utils/vuex'

export default {
    getDoctors: get('doctors'),
    getNurses: get('nurses'),
    getPatients: get('patients'),
    getBirths: get('births'),
    getDeaths: get('deaths'),
    getAppointments: get('appointments'),
    getNotifications: get('notifications'),

    getManagementCategories: get('managementCategories'),

    getMonitoringPatients: state => {
        return state.patients.filter(patient => {
            return patient.monitoring
        })
    },

    getHospitalizedPatients: state => {
        return state.patients.filter(patient => {
            return patient.hospitalized
        })
    },
    
    getEmergencyPatients: state => {
        return state.patients.filter(patient => {
            return patient.emergency
        })
    },

    getActivityCategories: state => {
        let categories = []
        if (state.activities) {
            state.activities.forEach(activity => {
                if (!categories.includes(activity['On'])) {
                    categories.push(activity['On'])
                }
            })
        }

        return categories
    },

    getActivityData: state => {
        if (state.activities) {
            let doctors = [];
            let patients = [];
            state.activities.forEach($activity => {
                if ($activity['On'] == 'doctors') {
                    doctors.push($activity)
                } else if ($activity['On'] == 'patients') {
                    patients.push($activity)
                }
            })
            return {
                'doctors': doctors,
                'patients': patients
            }
        }

        return null
    },
    getManagementData: state => {
        return {
            'doctors': state.doctors,
            'patients': state.patients,
            'nurses': state.nurses,
            'births': state.births,
            'deaths': state.deaths,
        }
    },
    getMonitoringPatientsIIII: state => {
        return state.patients.filter(patient => {
            return patient.monitoring && patient.temperature > 36 && patient.temperature < 38
        })
    },
    getMonitoringPatientsIII: state => {
        return state.patients.filter(patient => {
            return patient.monitoring && patient.temperature >= 38 && patient.temperature <= 39.5 ||
                patient.monitoring && patient.temperature > 35.5 && patient.temperature <= 36
        })
    },
    getMonitoringPatientsII: state => {
        return state.patients.filter(patient => {
            return patient.monitoring && patient.temperature > 39.5 && patient.temperature <= 41 ||
                patient.monitoring && patient.temperature >= 34 && patient.temperature <= 35.5
        })
    },
    getMonitoringPatientsI: state => {
        return state.patients.filter(patient => {
            return patient.monitoring && patient.temperature > 41 ||
                patient.monitoring && patient.temperature < 34
        })
    }
}