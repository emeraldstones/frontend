export default {
    //  Humans data
    doctors: null,
    nurses: null,
    patients: [],
    births: null,
    deaths: null,
    managementCategories: [
        'doctors', 'nurses', 'patients', 'births', 'deaths'
    ],
    notifications: [],
    activityCategories: [
        'doctors', 'patients'
    ],
    //  Activity data
    activities: null,
    //  Appointments
    appointments: null,
}
