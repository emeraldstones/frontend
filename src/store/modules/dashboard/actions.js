import axios from 'axios'
import { refresh, expired } from '@/utils/api'

export default {
    fetchData({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('dashboard')
            .then(response => {
                commit('SET_DOCTORS', response.data.doctors)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },
    
    fetchDoctors({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('doctors')
            .then(response => {
                commit('SET_DOCTORS', response.data.doctors)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },

    fetchPatients({ commit }, hospitalized = false) {
        return new Promise((resolve, reject) => {
            axios.get('patients', {
                hospitalized: hospitalized
            })
            .then(response => {
                commit('SET_PATIENTS', response.data.patients)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },

    fetchPatient({ commit }, payload) {
        return new Promise((resolve, reject) => {
            axios.get(`patient?monitoring=${payload.monitoring}&hospitalized=${payload.hospitalized}&emergency=${payload.emergency}`)
            .then(response => {
                commit('SET_PATIENTS', response.data.results)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },

    createPatient({ commit, dispatch }, { body }) {
        return new Promise((resolve, reject) => {
            axios.post(`store/patient`, body)
                .then(response => {

                    refresh(response, axios)
                    resolve(response)
                })
                .catch(error => {
                    expired(error, axios)
                })
        })
    },

    putPatient({ commit, dispatch }, {id, body}) {
        return new Promise((resolve, reject) => {
            axios.put(`patient/${id}`, body)
                .then(response => {

                    refresh(response, axios)
                    resolve(response)
                })
                .catch(error => {
                    expired(error, axios)
                })
        })
    },

    updatePatient({ commit, dispatch }, payload) {
        return new Promise((resolve, reject) => {
            // if (payload.monitoring) {
            //     commit('UPDATE_MONITORING_PATIENT', payload.patient)
            //     
            // }
            // if (payload.hospitalized) {
            //     commit('UPDATE_HOSPITALIZED_PATIENT', payload.patient)
            // }
            // if (payload.emergency) {
            //     commit('UPDATE_EMERGENCY_PATIENT', payload.patient)
            // } else {
            // }
            setTimeout(() => {
                commit('UPDATE_PATIENT', payload.patient)
                dispatch('app/updateSeverity', {} , { root: true })
            }, 1)
            setTimeout(() => {
                commit('UPDATE_EMERGENCY_PATIENT', payload.patient)
            }, 10)

            resolve()
        })
    },

    fetchNotifications({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get(`notifications`)
            .then(response => {
                commit('SET_NOTIFICATIONS', response.data.notifications)

                resolve(response)
            })
            .catch(error => {
            })
        })
    },

    storeNotification({ commit }, { body }) {
        return new Promise((resolve, reject) => {
            axios.post(`store/notification`, body)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
            })
        })
    },

    fetchActivities({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('activities')
            .then(response => {
                commit('SET_ACTIVITIES', response.data.activities)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },

    fetchAppointments({ commit }, day) {
        return new Promise((resolve, reject) => {
            axios.get(`appointment?day=${day}`)
            .then(response => {
                commit('SET_APPOINTMENTS', response.data.appointments)

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    },
}