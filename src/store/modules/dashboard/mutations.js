import Vue from 'vue'
import axios from 'axios'
import { set } from '@/utils/vuex'
var moment = require('moment');

export default {
    SET_DOCTORS: set('doctors'),
    SET_NURSES: set('nurses'),
    SET_MONITORING_PATIENTS: set('monitoringPatients'),
    SET_HOSPITALIZED_PATIENTS: set('hospitalizedPatients'),
    SET_BIRTHS: set('births'),
    SET_DEATHS: set('deaths'),
    SET_ACTIVITIES: set('activities'),
    SET_APPOINTMENTS: set('appointments'),

    SET_NOTIFICATIONS (state, notifications) {
        notifications.map((notification, key) => {
            notification.snackbar = true
        })
        state.notifications = notifications
    },
    SET_PATIENTS (state, patients) {
        patients.forEach(patient => {
            if (patient.emergency) {
                let waitingTime = 0
                if (patient.severity === 4) waitingTime = 30 * 60 * 1000
                if (patient.severity === 3) waitingTime = 15 * 60 * 1000
                if (patient.severity === 2) waitingTime = 5 * 60 * 1000
                if (patient.severity === 1) waitingTime = 1 * 60 * 1000
                
                patient.intervention = waitingTime - moment().diff(moment(patient.emergency_at))

                if (patient.intervention > 0) {
                    patient.timer = setInterval(() => {
                        patient.intervention -= 1000 
                        if (patient.intervention <= 0) {
                            console.log("SEND NOTIFICATION")
                            let color = ''
            
                            if (patient.severity == 1) {
                                color = '#F44336'
                            } else if (patient.severity == 2){
                                color = '#FF9800'
                            } else if (patient.severity == 3){
                                color = '#4CAF50'
                            } else if (patient.severity == 4){
                                color = '#42A5F5'
                            } else {
                                color = '#424242'
                            }

                            axios.post(`store/notification`, {
                                message: `${patient.human.first_name} ${patient.human.last_name} needs intervention`,
                                action: `emergency`,
                                color: color,
                            })
                            
                            if (patient.severity === 1) { patient.intervention = -4000 }
                            else if (patient.severity === 2) { patient.intervention = -3000 }
                            else if (patient.severity === 3) { patient.intervention = -2000 }
                            else if (patient.severity === 4) { patient.intervention = -1000 }
                            else { patient.intervention = 0 }
                            
                            clearInterval(patient.timer)
                        } 
                    }, 1000)
                } else {
                    if (patient.severity === 1) { patient.intervention = -4000 }
                    else if (patient.severity === 2) { patient.intervention = -3000 }
                    else if (patient.severity === 3) { patient.intervention = -2000 }
                    else if (patient.severity === 4) { patient.intervention = -1000 }
                    else { patient.intervention = 0 }
                    patient.notification = false
                } 
            }
        })
        state.patients = patients
    },

    UPDATE_PATIENT (state, patient) {
        const changedPatient = state.patients.filter(changedPatient => {
            return changedPatient.id === patient.id
        })
        if (changedPatient.length > 0) {
            const changedPatientKey = state.patients.indexOf(changedPatient[0])
            Object.assign(state.patients[changedPatientKey], patient)
        } else {
            state.patients.push(patient)
        }
    },

    UPDATE_EMERGENCY_PATIENT (state, patient) {
        const changedPatient = state.patients.filter(changedPatient => {
            return changedPatient.id === patient.id
        })[0]
        const changedPatientKey = state.patients.indexOf(changedPatient)

        if (changedPatient.emergency && changedPatient.emergency_at) {
            if (changedPatient.timer) {
                clearInterval(changedPatient.timer)
            }
            let waitingTime = 0
            if (changedPatient.severity === 4) waitingTime = 30 * 60 * 1000
            if (changedPatient.severity === 3) waitingTime = 15 * 60 * 1000
            if (changedPatient.severity === 2) waitingTime = 5 * 60 * 1000
            if (changedPatient.severity === 1) waitingTime = 1 * 60 * 1000

            let intervention = waitingTime - moment().diff(moment(patient.emergency_at))

            Vue.set(state.patients[changedPatientKey], 'intervention', intervention)
            
            if (changedPatient.intervention > 0) {
                changedPatient.timer = setInterval(() => {
                    changedPatient.intervention -= 1000 
                    if (changedPatient.intervention <= 0) {
                        console.log("SEND NOTIFICATION")
                        let color = ''
        
                        if (changedPatient.severity == 1) {
                            color = '#F44336'
                        } else if (changedPatient.severity == 2){
                            color = '#FF9800'
                        } else if (changedPatient.severity == 3){
                            color = '#4CAF50'
                        } else if (changedPatient.severity == 4){
                            color = '#42A5F5'
                        } else {
                            color = '#424242'
                        }

                        axios.post(`store/notification`, {
                            message: `${changedPatient.human.first_name} ${changedPatient.human.last_name} needs intervention`,
                            action: `emergency`,
                            color: color,
                        })

                        if (changedPatient.severity === 1) { changedPatient.intervention = -4000 }
                        else if (changedPatient.severity === 2) { changedPatient.intervention = -3000 }
                        else if (changedPatient.severity === 3) { changedPatient.intervention = -2000 }
                        else if (changedPatient.severity === 4) { changedPatient.intervention = -1000 }
                        else { changedPatient.intervention = 0 }
                        clearInterval(changedPatient.timer)
                    } 
                }, 1000)
            } else {
                if (changedPatient.severity === 1) { changedPatient.intervention = -4000 }
                else if (changedPatient.severity === 2) { changedPatient.intervention = -3000 }
                else if (changedPatient.severity === 3) { changedPatient.intervention = -2000 }
                else if (changedPatient.severity === 4) { changedPatient.intervention = -1000 }
                else { changedPatient.intervention = 0 }
            }
        }
    }
}
