export default {
    status: null,
    access_token: localStorage.getItem('access_token') || null,
    user: null
}
