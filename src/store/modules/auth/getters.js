import { get, doubleNot } from '@/utils/vuex'

export default {
    getAccessToken: get('access_token'),
    getAccessStatus: doubleNot('access_token'),
    getStatus: get('status'),
    getUser: get('user'),
    getAccessLevel: (state) => {
        if (state.user !== null) {
            return state.user.access_level
        }

        return null
    },
}
