import { auth_in, auth_out } from '@/utils/auth'
import { refresh, expired } from '@/utils/api'
import axios from 'axios'
import router from '@/router'
import Vue from 'vue'
import Echo from "laravel-echo"

export default {

    login({ commit, dispatch }, user) {
        return new Promise((resolve, reject) => {
            axios.post('auth/login', user)
            .then(response => {
                const access_token  = response.data.access_token
                const token_type    = response.data.token_type
                const user          = response.data.user
                const status        = 'logged-in'
                
                Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token

                window.Echo = new Echo({
                    broadcaster: 'socket.io',
                    host: 'https://backend.miutechs.com' + ':6001',
                    auth: 
                    {
                        headers:
                        {
                            'Authorization': 'Bearer ' + access_token
                        }
                    }
                })

                dispatch('dashboard/fetchPatient', { monitoring: true }, { root: true })
                    .then(() => {
                        dispatch('app/updateSeverity', {}, { root: true })
                    })
                
                window.Echo.channel('patientAction')
                    .listen('PatientUpdated', (e) => {
                        dispatch('dashboard/updatePatient', { patient: e.patient }, { root: true })
                    })

                auth_in(axios, access_token, token_type)
                commit('SET_LOGGED_IN', { access_token, status, user })
                resolve(response)
            })
            .catch(error => {
                const status = 'invalid-credentials'
                
                auth_out(axios)
                commit('SET_LOGGED_OUT', { status })

                reject(error.response.data)
            })
        })
    },

    logout({ commit }) {
        return new Promise((resolve, reject) => {
            axios.post('auth/logout', user)
            .then(response => {
                const status = 'logged-out'

                auth_out(axios)
                commit('SET_LOGGED_OUT', status)
                router.push({ path: '/login' })

                resolve(response)
            })
            .catch(error => {
                const status = 'error'

                auth_out(axios)
                commit('SET_LOGGED_OUT', status)
                router.push({ path: '/login' })

                reject(error)
            })

        })
    },

    refresh({ commit }) {
        return new Promise((resolve, reject) => {
            axios.post('auth/refresh')
            .then(response => {
                const access_token  = response.data.access_token
                const token_type    = response.data.token_type
                const user          = response.data.user
                let status          = response.data.status

                if (status === 'changed') {
                    status = 'token-refreshed'
                    auth_in(axios, access_token, token_type)
                }

                commit('SET_LOGGED_IN', { access_token, status, user })

                resolve(response)
            })
            .catch(error => {
                const status = 'unauthorized'
                auth_out(axios)
                commit('SET_LOGGED_OUT', { status })
                router.push({ path: '/login' })
            })
        })
    },

    update({ commit }, { body }) {
        return new Promise((resolve, reject) => {
            axios.put('auth/update', body)
            .then(response => {

                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })  
    }
}
