import { set } from '@/utils/vuex'

export default {
    SET_LOGGED_IN(state, { access_token, status, user }) {
        state.access_token  = access_token
        state.status        = status
        state.user          = user
    },

    SET_LOGGED_OUT(state, { status }) {
        state.access_token  = null
        state.status        = status
        state.user          = null
    },
}
