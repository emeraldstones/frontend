// https://vuex.vuejs.org/en/getters.html
import { get } from '@/utils/vuex'

export default {
    getBaseURL: get('baseURL'),
    getStorageURL: get('storageURL')
}
