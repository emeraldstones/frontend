// https://vuex.vuejs.org/en/state.html

export default {
    baseURL: 'https://backend.miutechs.com/api/',
    storageURL: 'https://backend.miutechs.com/storage/'
}
