import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate'
import DaySpanVuetify from 'dayspan-vuetify'

import animationSync from 'css-animation-sync';

//  Laravel echo events broadcast listener
import Echo from "laravel-echo"

//  ApexChart library
import VueApexCharts from 'vue-apexcharts'

//  Vue Carousel
import VueCarousel from 'vue-carousel';

//  Axios import
import axios from 'axios'

//  Components
import './components'

//  Plugins
import './plugins'

//  Sync router with store
import { sync } from 'vuex-router-sync'

//  Application import
import App from './App.vue'

//  Multiple languages
import i18n from '@/i18n'

//  Router & Store & Auth
import router from '@/router'
import store from '@/store'

//  Vuetify theme
import theme from '@/theme'

//  Style import
import 'vuetify/dist/vuetify.min.css'
import './assets/stylus/main.styl'
import './assets/scss/main.scss'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

//  MDI import
import '@mdi/font/css/materialdesignicons.css'

//  Sync store with router
sync(store, router)

//  Vue ApexCharts
Vue.use(VueApexCharts)

//  Vue Moment
Vue.use(require('vue-moment'))

//  Vue requirements
Vue.use(VeeValidate)
Vue.use(VueAxios, axios)

//  Axios baseURL
axios.defaults.baseURL = store.getters.getBaseURL

Vue.use(VueRouter)
Vue.use(Vuetify, { theme })

Vue.use(VueCarousel);

Vue.use(DaySpanVuetify, {
    methods: {
        getDefaultEventColor: () => '#1976d2'
    }
});

Vue.component('apexchart', VueApexCharts)

//  Vue production
Vue.config.productionTip = false

//  Socket.io config
window.io = require('socket.io-client')

if (store.getters['auth/getAccessStatus']) {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: 'https://backend.miutechs.com' + ':6001',
        auth: 
        {
            headers:
            {
                'Authorization': 'Bearer ' + store.getters['auth/getAccessToken']
            }
        }
    })
}

new Vue({
    i18n,
    router,
    store,
    render: h => h(App)
}).$mount('#app')