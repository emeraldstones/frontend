import Vue from 'vue'
export const auth_in = (axios, access_token, token_type) => {
    localStorage.setItem('access_token', access_token)
    //	Set Authorization header
    axios.defaults.headers.common['Authorization'] = token_type + ' ' + access_token
    Vue.prototype.$http.defaults.headers.common['Authorization'] = token_type + ' ' + access_token
}

export const auth_out = axios => {
    localStorage.removeItem('access_token')
    delete axios.defaults.headers.common['Authorization']
}
