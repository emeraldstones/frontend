import { isArray } from "util";

export const xml2json = (xml) => {

	// Create the return object
	let obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
			for (let j = 0; j < xml.attributes.length; j++) {
				let attribute = xml.attributes.item(j);
				obj[attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		// obj = xml.nodeValue;
	}

	// do children
	// If just one text node inside
	if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
		obj = xml.childNodes[0].nodeValue;
	}
	else if (xml.hasChildNodes()) {
		for(let i = 0; i < xml.childNodes.length; i++) {
			let item = xml.childNodes.item(i);
			let nodeName = item.nodeName;
			if (nodeName !== '#comment' && nodeName !== '#text') {
				if (typeof(obj[nodeName]) == "undefined") {
					obj[nodeName] = xml2json(item);
				} else {
					if (typeof(obj[nodeName].push) == "undefined") {
						let old = obj[nodeName];
						obj[nodeName] = [];
						obj[nodeName].push(old);
					}
					obj[nodeName].push(xml2json(item));
				}
			}
		}
	}
	return obj;
}

export const isObject = (object) => {
	return object && typeof object === 'object' && object.constructor === Object
}

const flattenObject = (data) => {
	let obj = {};

    for (let i in data) {
        if (!data.hasOwnProperty(i)) continue;

        if ((typeof data[i]) == 'object' && data[i] !== null) {
            let flatObject = flattenObject(data[i]);
            for (let x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;
				let xKey = x.split('_').length > 1 ? x.split('_')[1] : x
                obj[i.split('_').join(' ') + ' - ' + x] = flatObject[x];
            }
        } else {
            obj[i.split('_').join(' ')] = data[i];
        }
	}
	
    return obj;
}

export const data2array = (data) => {
	let obj = {}

	for (let element in data) {
		if (isObject(data[element])) {
			let flattenObj = data[element][Object.keys(data[element])[0]]
			obj[element] = []

			if (isObject(flattenObj)) {
				obj[element].push(flattenObject(flattenObj))
			} else if(Array.isArray(flattenObj)) {
				flattenObj.forEach(child => {
					obj[element].push(flattenObject(child))
				})
			}
		} else if(Array.isArray(data[element])) {
			obj[element] = []
			data[element].forEach(child => {
				obj[element].push(flattenObject(child))
			})
		}
	}
	return obj
}