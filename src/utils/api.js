//  Import store
import store from '@/store'

//  Import router
import router from '@/router'

//  Import auth utils
import { auth_in, auth_out } from '@/utils/auth'

export const refresh = (response, axios) => {
    if (response.data.auth.status === 'changed') {
        const access_token  = response.data.auth.access_token
        const token_type    = response.data.auth.token_type
        const user          = response.data.auth.user
        const status        = 'token-refreshed'

        auth_in(axios, access_token, token_type)
        store.commit('auth/SET_LOGGED_IN', { access_token, status, user })
    } 
}

export const expired = (error, axios) => {
    const status = 'token-expired'

    if (error.response.status === 401) {
        auth_out(axios)
        store.commit('auth/SET_LOGGED_OUT', { status })
        router.push({ path: '/login' })
    }
}

