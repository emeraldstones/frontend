//  Lib imports
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'

//  Import store
import store from '@/store'

// Routes
import paths from './paths'

function route(path, view, name, meta, children) {
    let newChild = []

    if (typeof children !== 'undefined' && children.length > 0) {
        newChild = children.map(child => route(child.path, child.view, child.name, child.meta, child.children))
    }

    const newRoute = {
        name: name || view,
        path,
        component: resolve => import(`@/views/${view}.vue`).then(resolve),
        meta: meta || {},
        children: newChild || [],
    }

    return newRoute
}

Vue.use(VueRouter)

// Create a new router
const router = new VueRouter({
    mode: 'history',
    routes: paths
        .map(path => route(path.path, path.view, path.name,  path.meta, path.children))
        .concat([{ path: '*', redirect: '/' }]),
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        }
        if (to.hash) {
            return { selector: to.hash }
        }
        return { x: 0, y: 0 }
    }
})

Vue.use(Meta)

// Bootstrap Analytics
// Set in .env
// https://github.com/MatteoGabriele/vue-analytics
if (process.env.GOOGLE_ANALYTICS) {
    Vue.use(VueAnalytics, {
        id: process.env.GOOGLE_ANALYTICS,
        router,
        autoTracking: {
            page: process.env.NODE_ENV !== 'development'
        }
    })
}

router.beforeEach((to, from, next) => {
    const requiresAuth  = to.matched.some(record => record.meta.requiresAuth)
    const requiresAccess = to.matched.some(record => record.meta.access_level)
    const isLoggedIn    = store.getters['auth/getAccessStatus']

    let path_access_level = -1;
    to.matched.some(record => {
        if(requiresAccess) {
            path_access_level = record.meta.access_level
        }
    })

    if (requiresAuth) {
        if (isLoggedIn) {
            //	Set Authorization header
            Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + store.getters['auth/getAccessToken']
            
            if (!from.name) {
                store.dispatch('app/setRouteChanging', true)
                store.dispatch('auth/refresh')
                    .then(response => {
                        store.dispatch('app/setRouteChanging', false)

                        const access_level = store.getters['auth/getAccessLevel']
                        
                        if (requiresAccess) {
                            let ok = true

                            if (access_level < path_access_level) {
                                ok = false
                            }
                            if (!ok) {
                                if (from.path === '/') {
                                    next(false)
                                } else {
                                    next('/')
                                }
                            } else {
                                next()
                            }
                        } else {
                            next()
                        }
                    })
            } else {
                const access_level = store.getters['auth/getAccessLevel']
                        
                if (requiresAccess) {
                    let ok = true

                    if (access_level < path_access_level) {
                        ok = false
                    }
                    if (!ok) {
                        if (from.path === '/') {
                            next(false)
                        } else {
                            next('/')
                        }
                    } else {
                        next()
                    }
                } else {
                    next()
                }
            }
            
        } else {
            if (from.path === '/login') {
                next(false)
            } else {
                next('/login')
            }
        }
    } else {
        if (isLoggedIn) {
            if(to.path === '/login' || to.path === '/register') {
                next('/')       //  Redirect if user is logged in
            } else {
                next()
            }
        } else {
            next()
        }
    }
})

export default router
