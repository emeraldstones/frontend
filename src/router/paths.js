/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

export default [
    {
        path: '/',
        view: 'Home',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        view: 'Auth/Login'
    },
    {
        path: '/register',
        view: 'Auth/Register'
    },
    {
        path: '/unauthorized',
        view: 'Auth/Unauthorized'
    },
    {
        path: '/dashboard',
        view: 'Dashboard',
        meta: {
            requiresAuth: true,
            access_level: 2
        }
    },
    {
        path: '/hospital',
        view: 'Hospital',
        meta: {
          requiresAuth: true,
          access_level: 2
        }
    },
    {
        path: '/emergency',
        view: 'Emergency',
        meta: {
          requiresAuth: true,
          access_level: 2
        }
    },
    {
        path: '/monitoring',
        view: 'Monitoring',
        meta: {
          requiresAuth: true,
          access_level: 2
        }
    },
    {
        path: '/care',
        view: 'Care',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/patients/:id',
        view: 'Patients',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/process/patients/:id',
        view: 'Process',
        meta: {
            requiresAuth: true
        }
    }
]
    //  Home
    // {
    //     path: '/',
    //     view: 'Home',
    //     meta: {
    //         requiresAuth: true
    //     },
    //     children: [
    //         {
    //             path: '/users/:id',
    //             view: 'User'
    //         }
    //     ]
    // },
    // //  Auth paths
    // {
    //     path: '/register',
    //     view: 'Auth/Register',
    //     meta: {
    //       requiresAuth: false
    //     }
    // },
    // {
    //     path: '/login',
    //     view: 'Auth/Login',
    //     meta: {
    //       requiresAuth: false
    //     }
    // },
    // {
    //     path: '/dashboard',
    //     view: 'Dashboard',
    //     meta: {
    //       requiresAuth: true,
    //       roles: 4
    //     }
    // },
    // {
    //     path: '/care',
    //     view: 'Care',
    //     meta: {
    //       requiresAuth: false
    //     }
    // },
    // {
    //     path: '/hospital',
    //     view: 'Hospital',
    //     meta: {
    //       requiresAuth: false
    //     }
    // },
    // {
    //     path: '/pharmacy',
    //     view: 'Pharmacy',
    //     meta: {
    //       requiresAuth: false
    //     }
    // }
// ]
