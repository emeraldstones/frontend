import axios from 'axios'
import { refresh, expired } from '@/utils/api'

const doctors = {

    fetch(query) {
        return new Promise((resolve, reject) => {
            axios.get(`doctor${query}`)
            .then(response => {
                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    }
}

export default doctors