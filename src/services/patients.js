import axios from 'axios'
import { refresh, expired } from '@/utils/api'

const patients = {

    fetch(query = '') {
        return new Promise((resolve, reject) => {
            axios.get(`patient${query}`)
            .then(response => {
                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    }
}

export default patients