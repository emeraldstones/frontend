import axios from 'axios'
import { refresh, expired } from '@/utils/api'

const appointments = {

    fetch(query) {
        return new Promise((resolve, reject) => {
            axios.get(`appointment${query}`)
            .then(response => {
                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    }
}

export default appointments