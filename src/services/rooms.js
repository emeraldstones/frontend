import axios from 'axios'
import { refresh, expired } from '@/utils/api'

const rooms = {

    fetch(query = '') {
        return new Promise((resolve, reject) => {
            axios.get(`bed/available${query}`)
            .then(response => {
                refresh(response, axios)
                resolve(response)
            })
            .catch(error => {
                expired(error, axios)
            })
        })
    }
}

export default rooms